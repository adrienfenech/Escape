package com.game.yolo.escape.Interface;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.view.View;

import com.game.yolo.escape.R;

import java.util.ArrayList;

/**
 * Created by yolo on 8/20/14.
 */
public class Menu extends View {
    public Menu(Context context) {
        super(context);
        icon = prepareBitmap(getResources().getDrawable(R.drawable.exit), iconSize, iconSize);
        posX = 0;
        posY = 0;
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawBitmap(icon, posX, posY, null);
    }

    protected static Bitmap prepareBitmap(Drawable drawable, int width, int height) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        drawable.setBounds(0, 0, width, height);
        Canvas canvas = new Canvas(bitmap);
        drawable.draw(canvas);
        return bitmap;
    }

    public void setDimension(int width, int height) {
        this.width = width;
        this.height = height;
        resetPosition();
    }

    public void resetPosition() {
        posX = 0;
        posY = height - iconSize;
    }

    /** Get / Set
     * posX / posY
     * prevX / prevY
     * deltaX / deltaY
     * sizeIcon
     */
    public float getPosX() { return posX; }
    public void setPosX(float posX) { this.posX = posX; }

    public float getPosY() { return posY; }
    public void setPosY(float posY) { this.posY = posY; }

    public float getDeltaX() { return deltaX; }
    public void setDeltaX(float deltaX) { this.deltaX = deltaX; }

    public int getIconSize() { return iconSize; }


    /** A/D
     * icon     : The sprite of the Player
     * iconSize : The size of the sprite
     * posX     : Position on X axis of the Player
     * posY     : Position on Y axis of the Player
     * deltaX   : Difference between the X position pressed and the X position of the sprite
     * width    : Width of the screen
     * height   : Height of the screen
     */
    protected Bitmap icon;
    protected int iconSize = 128;

    protected float posX;
    protected float posY;
    protected float deltaX;
    private int width;
    private int height;
}
