package com.game.yolo.escape;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

/**
 * Created by yolo on 8/13/14.
 */
public class MainThread extends Thread {
    public MainThread(SurfaceHolder surfaceHolder, Game game) {
        super();
        this.surfaceHolder = surfaceHolder;
        this.game = game;
    }

    @Override
    public void run() {
        Canvas canvas;
        long beginTime;
        long timeDiff;
        int sleepTime;

        while (running) {
            canvas = null;
            try {
                canvas = this.surfaceHolder.lockCanvas();
                synchronized (surfaceHolder) {
                    beginTime = System.currentTimeMillis();

                    this.game.update();
                    this.game.onDraw(canvas);
                    timeDiff = System.currentTimeMillis() - beginTime;
                    sleepTime = (int)(this.FP - timeDiff);
                    if (sleepTime > 0) {
                        try {
                            Thread.sleep(sleepTime);
                        }
                        catch (InterruptedException e) {}
                    }
                }
            }
            finally {
                if (canvas != null) {
                    surfaceHolder.unlockCanvasAndPost(canvas);
                }
            }

        }
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    /** A/D
     * FPS              : Frame Per Second
     * FP               : Frame Period
     * surfaceHolder    : The surface
     * game             : The game
     * running          : The game is running or not
     */

    private final static int FPS = 30;
    private final static int FP = 1000 / FPS;
    private SurfaceHolder surfaceHolder;
    private Game game;
    private boolean running;
}
