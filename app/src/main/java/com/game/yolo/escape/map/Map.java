package com.game.yolo.escape.map;

import android.content.Context;
import android.graphics.Canvas;

import java.util.ArrayList;

/**
 * Created by yolo on 8/13/14.
 */
public class Map {
    public Map(Context context) {
        this.context = context;
        decal = 0;
    }

    public void update() {
        for (Tile tile : map)
            tile.update();
    }

    public void onDraw(Canvas canvas) {
        for (Tile tile : map)
            tile.onDraw(canvas);
    }

    public ArrayList<Tile> getMap() { return map; }

    public Tile getTile(int i, int j) { return map.get(j * mapWidth + i); }

    public void setDimension(int width, int height) {
        this.width = width;
        this.height = height;
        createGameMap();
    }

    public void createGameMap() {
        this.mapWidth = width / tileSize + 1;
        this.mapHeight = height / tileSize * 5 / 3;
        System.out.println("MAP: " + mapWidth + "," + mapHeight);
        this.map = new ArrayList<Tile>();
        for (int j = 0; j < mapHeight; j++) {
            for (int i = 0; i < mapWidth; i++) {
                float posX = i * tileSize - decal;
                float posY = j * tileSize * 2 / 3;
                map.add(new Tile(this.context, posX, posY));
            }
            decal = decal == 16 ? 0 : 16;
        }
    }

    public void doReaction(float posX, float posY) {
        int x = (int)(posX / tileSize);
        int y = (int)(3 * posY / (tileSize * 2));
        int jDown = Math.max(0, y - 6);
        int jUp = Math.min(mapHeight, y + 6);
        int iDown = Math.max(0, x - 6);
        int iUp = Math.min(mapWidth, x + 6);
        for (int j = jDown; j < jUp; j++) {
            for (int i = iDown; i < iUp; i++) {
                if (!getTile(i, j).isWay) {
                    float dist = (float) Math.sqrt(Math.pow(i - x, 2) + Math.pow(j - y, 2));
                    float alpha = 255 / (dist * 4);
                    if (alpha > 10)
                        getTile(i, j).setAlphaCanvas((int) alpha);
                }
            }
        }
        if (x >= 0 && x < mapWidth && y >= 0 && y < mapHeight)
            setWay(x, y);
    }
    public void setWay(int i, int j) {
        getTile(i, j).isWay = true;
        getTile(i, j).setAlphaMax();
    }

    public void resetTile() {
        for (Tile t : map)
            if (!t.isWay)
                t.resetAlphaCanvas();
    }

    /** A/D
     * map          : The array of hexagon's tile
     * tileSize     : Size of the tile
     * decal        : Value of the decal when the map is created
     * mapWidth     : Width of the Map
     * mapHeight    : Height of the Map
     * width        : Width of the screen
     * height       : Height of the screen
     * context      : Context of the game
     */

    ArrayList<Tile> map;
    int tileSize = 32;
    int decal;
    int mapWidth;
    int mapHeight;
    int width;
    int height;
    Context context;
}
