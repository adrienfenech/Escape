package com.game.yolo.escape.unit;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.view.View;

import com.game.yolo.escape.R;

import java.util.ArrayList;

/**
 * Created by yolo on 8/13/14.
 */
public class Player extends View {
    public Player(Context context) {
        super(context);
        icon1 = prepareBitmap(getResources().getDrawable(R.drawable.sphere1), iconSize, iconSize);
        icon2 = prepareBitmap(getResources().getDrawable(R.drawable.sphere2), iconSize, iconSize);
        icon3 = prepareBitmap(getResources().getDrawable(R.drawable.arc), iconSize, iconSize);
        posX = 0;
        posY = 0;

        angle1 = 180;
        angle2 = 0;
    }

    public void update() {
        angle1 -= 6;
        angle1 %= 360;

        angle2 += 3;
        angle2 %= 360;

    }

    public void onDraw(Canvas canvas, float angle) {
        super.onDraw(canvas);

        Paint paint = new Paint();

        paint.setAlpha(255);
        canvas.save(); //save the position of the canvas
        canvas.rotate(angle1, posX + (iconSize / 2) + 1, posY + (iconSize / 2) + 1); //rotate the canvas' matrix
        canvas.drawBitmap(icon1, posX, posY, paint);
        canvas.restore();

        paint.setAlpha(150);
        canvas.save(); //save the position of the canvas
        canvas.rotate(angle2, posX + (iconSize / 2) - 1, posY + (iconSize / 2) - 1); //rotate the canvas' matrix
        canvas.drawBitmap(icon2, posX, posY, paint);
        canvas.restore();

        paint.setAlpha(150);
        canvas.save(); //save the position of the canvas
        canvas.rotate(angle, posX + (iconSize / 2), posY + (iconSize / 2)); //rotate the canvas' matrix
        canvas.drawBitmap(icon3, posX, posY, paint);
        canvas.restore();
    }

    public void partialInvalidate() {
        final int minX = (int) Math.min(posX, prevX);
        final int minY = (int) Math.min(posY, prevY);
        final int maxX = (int) Math.max(posX + iconSize, prevX + iconSize);
        final int maxY = (int) Math.max(posY + iconSize, prevY + iconSize);

        invalidate(minX, minY, maxX, maxY);
    }

    protected static Bitmap prepareBitmap(Drawable drawable, int width, int height) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        drawable.setBounds(0, 0, width, height);
        Canvas canvas = new Canvas(bitmap);
        drawable.draw(canvas);
        return bitmap;
    }

    public void setDimension(int width, int height) {
        this.width = width;
        this.height = height;
        resetPosition();
    }

    public void resetPosition() {
        posX = width / 2 - iconSize / 2;
        posY = height / 2 - iconSize / 2;
    }

    /** Get / Set
     * posX / posY
     * prevX / prevY
     * deltaX / deltaY
     * sizeIcon
     */
    public float getPosX() { return posX; }
    public void setPosX(float posX) { this.posX = posX; }

    public float getPosY() { return posY; }
    public void setPosY(float posY) { this.posY = posY; }

    public float getPrevX() { return prevX; }
    public void setPrevX(float prevX) { this.prevX = prevX; }

    public float getPrevY() { return prevY; }
    public void setPrevY(float prevY) { this.prevY = prevY; }

    public float getDeltaX() { return deltaX; }
    public void setDeltaX(float deltaX) { this.deltaX = deltaX; }

    public float getDeltaY() { return deltaY; }
    public void setDeltaY(float deltaY) { this.deltaY = deltaY; }

    public int getIconSize() { return iconSize; }

    /** A/D
     * icon     : The sprite of the Player
     * sizeIcon : The size of the sprite
     * posX     : Position on X axis of the Player
     * posY     : Position on Y axis of the Player
     * prevX    : Previous position on X axis of the Player
     * prevY    : Previous position on Y axis of the Player
     * deltaX   : Difference between the X position pressed and the X position of the sprite
     * deltaY   : Difference between the Y position pressed and the Y position of the sprite
     * width    : Width of the screen
     * height   : Height of the screen
     * count    : For drawing every sprite
     */
    protected Bitmap icon1;
    protected Bitmap icon2;
    protected Bitmap icon3;
    protected int iconSize = 128;

    protected float posX;
    protected float posY;
    protected float prevX;
    protected float prevY;

    protected float angle1;
    protected float angle2;

    protected float deltaX;
    protected float deltaY;

    private int width;
    private int height;
}
