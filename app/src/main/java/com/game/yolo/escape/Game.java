package com.game.yolo.escape;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.game.yolo.escape.Interface.Background;
import com.game.yolo.escape.Interface.Menu;
import com.game.yolo.escape.map.Map;
import com.game.yolo.escape.unit.Player;
import com.game.yolo.escape.unit.Unit;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by yolo on 8/13/14.
 */
public class Game extends SurfaceView implements SurfaceHolder.Callback {
    public Game(Context context) {
        super(context);
        this.context = context;
        angle = 0;
        rand = new Random();


        player = new Player(context);
        map = new Map(context);
        units = new ArrayList<Unit>();
        units.add(new Unit(context, rand));
        menu = new Menu(context);
        bg = new Background(context);
        //this.iconSize = player.getIconSize();

        getHolder().addCallback(this);
        thread = new MainThread(getHolder(), this);
        setFocusable(true);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.lastEvent = event;
        return super.onTouchEvent(event);
    }

    public boolean action() {
        final int action = lastEvent.getAction();
        final float x = lastEvent.getX();
        final float y = lastEvent.getY();
        map.doReaction(x, y);
        float posX = player.getPosX();
        float posY = player.getPosY();

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                if (x > posX
                        && x < posX + iconSize
                        && y > posY
                        && y < posY + iconSize) {
                    player.setDeltaX(x - posX);
                    player.setDeltaY(y - posY);
                    player.setPrevX(posX);
                    player.setPrevY(posY);
                    isOnPlayer = true;
                    return true;
                }
                else if (x > menu.getPosX()
                        && x < menu.getPosX() + menu.getIconSize()
                        && y > menu.getPosY()
                        && y < menu.getPosY() + menu.getIconSize()) {
                    isOnExit = true;
                    menu.setDeltaX(x - menu.getPosX());
                    return true;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if (isOnPlayer) {
                    player.setPrevX(posX);
                    player.setPrevY(posY);
                    posX = x - player.getDeltaX();
                    posY = y - player.getDeltaY();
                    /*if (posX < 0)
                        player.setPosX(0);
                    else if (posX > width - player.getIconSize())
                        player.setPosX(width - player.getIconSize());
                    else
                        player.setPosX(posX);
                    if (posY < 0)
                        player.setPosY(0);
                    else if (posY > height - player.getIconSize())
                        player.setPosY(height - player.getIconSize());
                    else
                        player.setPosY(posY);*/
                }
                else if (isOnExit) {
                    if (x - menu.getDeltaX() < 0)
                        menu.setPosX(0);
                    else
                        menu.setPosX(x - menu.getDeltaX());
                    float percent = (menu.getPosX() + 1) / width * 255;
                    //backGround = Color.argb(255, (int)percent, 0, 0);
                }
                angle = calculateAngle(x, y);
                return true;
            case MotionEvent.ACTION_UP:
                isOnPlayer = false;
                if (isOnExit && menu.getPosX() >= width - menu.getIconSize()) {
                    System.out.println("GAME FINISHED");
                    thread.setRunning(false);
                    ((Activity)getContext()).finish();
                }
                else if (isOnExit) {
                    menu.setPosX(0);
                    //backGround = Color.argb(255, 0, 0, 0);
                }
                return true;
        }
        return true;
    }

    private float calculateAngle(float x, float y) {
        float posX = player.getPosX() + player.getIconSize() / 2;
        float posY = player.getPosY() + player.getIconSize() / 2;

        float asX = x;
        float asY = posY;

        float hyp = (float)Math.sqrt(Math.pow(x - posX, 2) + Math.pow(y - posY, 2));
        float adj = (float)Math.sqrt(Math.pow(asX - posX, 2) + Math.pow(asY - posY, 2));

        float temp = (float)Math.toDegrees(Math.acos(adj / hyp));
        if (y < posY)
            temp *= -1;
        if (x < posX)
            temp = 180 - temp;
        return temp;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        thread.setRunning(true);
        thread.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // Clean shutdown
        // thread.setRunning(false);
        // ((Activity)getContext()).finish();

        boolean down = true;
        while (down) {
            try {
                thread.join();
                down = false;
            } catch (InterruptedException e) {
                // Try again
            }
        }
    }

    public void update() {
        if (!firstDrawn) {
            map.resetTile();
            if (lastEvent != null)
                action();
            player.update();
            for (int i = 0; i < units.size() - 1; i++) {
                units.get(i).update();
                map.doReaction(units.get(i).getPosX() + 32, units.get(i).getPosY() + 32);
            }
            map.update();
        }
        if (units.get(units.size() - 1).update() && units.size() < 10) {
            units.add(new Unit(context, rand));
            units.get(units.size() - 1).setDimension(width, height);
        }
        //System.out.println(player.getPosX());
        //map.update();
        //particleEngine.update();
        //player.update();
        //unit.update();
    }

    public void onDraw(Canvas canvas) {
        if (firstDrawn) {
            System.out.println("DIMENSION " + getWidth() + "," + getHeight());
            width = getWidth();
            height = getHeight();
            firstDrawn = false;
            units.add(new Unit(context, rand));
            units.get(units.size() - 1).setDimension(width, height);
            deployDimension();
        }
        else {
            //bg.onDraw(canvas);
            canvas.drawColor(Color.BLACK);
            map.onDraw(canvas);
            for (Unit u : units)
                u.onDraw(canvas);
            player.onDraw(canvas, angle);
            //menu.onDraw(canvas, backGround);
            menu.onDraw(canvas);
        }
    }

    /** Get/Set
     * getPlayer    : Get the player of the game
     */

    public Player getPlayer() { return this.player; }

    public void deployDimension() {
        player.setDimension(width, height);
        map.setDimension(width, height);
        units.get(0).setDimension(width, height);
        menu.setDimension(width, height);
        bg.setDimension(width, height);
    }

    /**
     * A/D
     * thread       : Main thread for the game loop and draw
     * player       : The player
     * map          : Map of the game
     * menu         : The game's menu
     * isOnPlayer   : Check if the player's ACTION DOWN is on the sprite or not
     * isOnExit   : Check if the player's ACTION DOWN is on the exit or not
     * lastEvent    : Last event from the player
     * width        : Width of the screen
     * height       : Height of the screen
     * iconSize     : Size of the player's sprite
     * firstDrawn   : First draw or not
     */

    private MainThread thread;
    private Player player;
    private Map map;
    private ArrayList<Unit> units;
    private Menu menu;
    private boolean isOnPlayer = false;
    private boolean isOnExit = false;
    private MotionEvent lastEvent;
    private int width;
    private int height;
    private int iconSize = 128;
    private boolean firstDrawn = true;
    private Background bg;
    private Random rand;
    private Context context;
    private float angle;

    //int backGround = Color.argb(255, 0, 0, 0);
}
