package com.game.yolo.escape.map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.view.View;

import com.game.yolo.escape.R;

import java.util.Random;

/**
 * Created by yolo on 8/13/14.
 */
public class Tile extends View {
    public Tile(Context context, float posX, float posY) {
        super(context);
        icon = prepareBitmap(getResources().getDrawable(R.drawable.hexagon4), iconSize, iconSize);
        paint = new Paint();
        this.posX = posX;
        this.posY = posY;
        alpha = 3;
        isWay = false;
    }

    public void update() {
        if (isWay) {
            if (alpha <= 10) {
                isWay = false;
                alpha = 3;
            }
            else
                this.alpha -= 5;
        }
    }

    public void onDraw(Canvas canvas) {
        paint.setAlpha(alpha);
        canvas.drawBitmap(icon, posX, posY, paint);
    }

    protected static Bitmap prepareBitmap(Drawable drawable, int width, int height) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        drawable.setBounds(0, 0, width, height);
        Canvas canvas = new Canvas(bitmap);
        drawable.draw(canvas);
        return bitmap;
    }

    public void setDimension(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void setAlphaCanvas(int alpha) {
        int temp = this.alpha + alpha;
        this.alpha = temp > 255 ? 255 : temp;
    }

    public void setAlphaMax() { this.alpha = 255; }

    public void resetAlphaCanvas() { this.alpha = 3; }


    /** A/D
     * icon     : The sprite of the Player
     * sizeIcon : The size of the sprite
     * posX     : Position on X axis of the Player
     * posY     : Position on Y axis of the Player
     * width    : Width of the screen
     * height   : Height of the screen
     * paint    : Paint for the canvas
     * alpha    : The alpha value for the canvas
     * isWay    : Tell if the hexagon is a part of a way
     */
    protected Bitmap icon;
    protected int iconSize = 32;

    protected float posX = 0;
    protected float posY = 0;

    private int width;
    private int height;

    Paint paint;
    int alpha;

    boolean isWay;
}
