package com.game.yolo.escape.unit;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.View;

import com.game.yolo.escape.R;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by yolo on 8/13/14.
 */
public class Unit extends View {
    public Unit(Context context, Random rand) {
        super(context);
        this.rand = rand;
        int temp = rand.nextInt(4);
        if (temp == 0)
            icon = prepareBitmap(getResources().getDrawable(R.drawable.cell1), iconSize, iconSize);
        else if (temp == 1)
            icon = prepareBitmap(getResources().getDrawable(R.drawable.cell2), iconSize, iconSize);
        else if (temp == 2)
            icon = prepareBitmap(getResources().getDrawable(R.drawable.cell3), iconSize, iconSize);
       else
            icon = prepareBitmap(getResources().getDrawable(R.drawable.cell4), iconSize, iconSize);
        velocity = (float)0.1;
    }

    public boolean update() {
        posX += (endX - startX) / (100 * velocity);
        posY += (endY - startY) / (100 * velocity);

        if (endX - posX < 5 && endY - posY < 5) {
            setMovement();
            return true;
        }
        return false;
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawBitmap(icon, posX, posY, null);
    }

    public void setMovement() {
        while (velocity < 0.5)
            velocity = rand.nextFloat();
        int temp = rand.nextInt(4);
        if (temp == 0) {
            startX = -128;
            startY = generateRandomNumber(0, height - iconSize);
            endX = width + 128;
            endY = generateRandomNumber(0, height - iconSize);
        }
        else if (temp == 1) {
            startX = width + 128;
            startY = generateRandomNumber(0, height - iconSize);
            endX = -128;
            endY = generateRandomNumber(0, height - iconSize);
        }
        else if (temp == 2) {
            startX = generateRandomNumber(0, width - iconSize);
            startY = -128;
            endX = generateRandomNumber(0, width - iconSize);
            endY = height + 128;
        }
        else {
            startX = generateRandomNumber(0, width - iconSize);
            System.out.println(width - iconSize + "  ||  " + startX);
            startY = height + 128;
            endX = generateRandomNumber(0, width - iconSize);
            endY = -128;
        }
        posX = startX;
        posY = startY;
        endX = width / 2 - iconSize / 2;
        endY = height / 2 - iconSize / 2;
    }

    public float generateRandomNumber(int min, int max) {
        return rand.nextFloat() * max;
    }

    protected static Bitmap prepareBitmap(Drawable drawable, int width, int height) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        drawable.setBounds(0, 0, width, height);
        Canvas canvas = new Canvas(bitmap);
        drawable.draw(canvas);
        return bitmap;
    }

    public void setDimension(int width, int height) {
        this.width = width;
        this.height = height;
        resetPosition();
        setMovement();
    }

    public void resetPosition() {
        posX = 0;
        posY = 0;
    }

    /** Get / Set
     * posX / posY
     * prevX / prevY
     * deltaX / deltaY
     * sizeIcon
     */
    public float getPosX() { return posX; }
    public void setPosX(float posX) { this.posX = posX; }

    public float getPosY() { return posY; }
    public void setPosY(float posY) { this.posY = posY; }

    public float getPrevX() { return prevX; }
    public void setPrevX(float prevX) { this.prevX = prevX; }

    public float getPrevY() { return prevY; }
    public void setPrevY(float prevY) { this.prevY = prevY; }

    public float getDeltaX() { return deltaX; }
    public void setDeltaX(float deltaX) { this.deltaX = deltaX; }

    public float getDeltaY() { return deltaY; }
    public void setDeltaY(float deltaY) { this.deltaY = deltaY; }

    public int getIconSize() { return iconSize; }

    /** A/D
     * icon     : The sprite of the Player
     * sizeIcon : The size of the sprite
     * posX     : Position on X axis of the Player
     * posY     : Position on Y axis of the Player
     * prevX    : Previous position on X axis of the Player
     * prevY    : Previous position on Y axis of the Player
     * deltaX   : Difference between the X position pressed and the X position of the sprite
     * deltaY   : Difference between the Y position pressed and the Y position of the sprite
     * width    : Width of the screen
     * height   : Height of the screen
     */
    protected Bitmap icon;
    protected int iconSize = 64;

    protected float posX = 0;
    protected float posY = 0;
    protected float prevX;
    protected float prevY;

    protected float startX = 0;
    protected float startY = 0;
    protected float endX = 1024;
    protected float endY = 1024;

    protected float deltaX;
    protected float deltaY;

    private int width;
    private int height;

    private float velocity;

    Random rand;
}
