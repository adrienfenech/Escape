package com.game.yolo.escape.Interface;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.View;

import com.game.yolo.escape.R;

/**
 * Created by yolo on 8/20/14.
 */
public class Background extends View {
    public Background(Context context) {
        super(context);
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawBitmap(icon, 0, 0, null);
    }

    protected static Bitmap prepareBitmap(Drawable drawable, int width, int height) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        drawable.setBounds(0, 0, width, height);
        Canvas canvas = new Canvas(bitmap);
        drawable.draw(canvas);
        return bitmap;
    }

    public void setDimension(int width, int height) {
        this.width = width;
        this.height = height;
        resetPosition();
    }

    public void resetPosition() {
        icon = prepareBitmap(getResources().getDrawable(R.drawable.background), width, height);
    }

    /** A/D
     * icon     : The sprite of the Background
     * width    : Width of the screen
     * height   : Height of the screen
     */
    protected Bitmap icon;
    private int width;
    private int height;
}
