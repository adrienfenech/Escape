package com.app.yolo.paneslibrarytest.menu;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.app.yolo.paneslibrarytest.NormalFragment;
import com.app.yolo.paneslibrarytest.R;
import com.app.yolo.paneslibrarytest.androidpaneslibrary.paneslayout.FragmentLauncher;
import com.app.yolo.paneslibrarytest.androidpaneslibrary.paneslayout.PanesActivity;
import com.app.yolo.paneslibrarytest.tools.VData;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by yolo on 10/15/14.
 */
public class MainMenuFragment extends Fragment {
    public MainMenuFragment() { super(); }
    public MainMenuFragment(String collection) {
        super();
        this.collection = collection;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setRetainInstance(true);

        parentView = inflater.inflate(R.layout.main_menu, container, false);
        parentView.setBackgroundColor(Color.WHITE);
        Random rand = new Random();

        ArrayList<MainMenuItem> items = new ArrayList<MainMenuItem>();
        for (int i = 0; i < 10; i++)
            items.add(new MainMenuItem(this.getActivity(), collection + "item " + i, "null", rand.nextInt(100)));
        final MyMainMenuAdapter adapter = new MyMainMenuAdapter(this.getActivity(), items);

        listView = (ListView) parentView.findViewById(R.id.main_menu_list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Fragment child;
                if ((Integer)VData.GET().getVData(VData.DEPHT) >= 3)
                    child = new NormalFragment();
                else
                    child = new MainMenuFragment(((MainMenuItem) (parent.getAdapter().getItem(position))).getText() + " -> ");
                Activity a = getActivity();
                if (a != null && a instanceof FragmentLauncher)
                    ((FragmentLauncher) a).addFragment(mine, child);
                Toast.makeText(getActivity(), "Click on item |" + position + "| Depht " + (Integer)VData.GET().getVData(VData.DEPHT) + "|", Toast.LENGTH_LONG).show();
            }
        });

        parentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        return parentView;
    }

    /** A/D
     * parentView       : Main view of the fragment
     * listView         : List of the main menu
     * panesActivity    : Current PanesActivity
     * mine             : Parent MenuFragment
     * collection       : Hint for next menu
     */
    private View                parentView;
    private ListView            listView;
    private MainMenuFragment    mine = this;
    private String              collection = "";
}
