package com.app.yolo.paneslibrarytest;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by yolo on 10/16/14.
 */
public class NormalFragment extends Fragment {
    public NormalFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setRetainInstance(true);
        parentView = inflater.inflate(R.layout.normal_activity, container, false);

        return parentView;
    }

    /** A/D
     * parentView       : Actual view
     */
    private View    parentView;
}
