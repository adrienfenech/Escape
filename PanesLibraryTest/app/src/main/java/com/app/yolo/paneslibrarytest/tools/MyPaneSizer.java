package com.app.yolo.paneslibrarytest.tools;

import com.app.yolo.paneslibrarytest.ExampleFragment;
import com.app.yolo.paneslibrarytest.NormalFragment;
import com.app.yolo.paneslibrarytest.androidpaneslibrary.paneslayout.PanesSizer;
import com.app.yolo.paneslibrarytest.menu.MainMenuFragment;
import com.app.yolo.paneslibrarytest.menu.ShortMenuFragment;

/**
 * Created by yolo on 10/15/14.
 */
public class MyPaneSizer implements PanesSizer.PaneSizer {
    private static final int UNKNOWN_PANE_TYPE = -1;
    private static final int DEFAULT_PANE_TYPE = 0;
    private static final int MENU_PANE_TYPE = 1;
    private static final int MAIN_MENU_PANE_TYPE = 2;
    private static final int SHORT_MENU_PANE_TYPE = 3;
    private static final int NORMAL_PANE_TYPE = 4;
    private static final int NORMAL_SHORT_PANE_TYPE = 5;

    @Override
    public int getWidth(int index, int size, int type, int parentWidth, int parentHeight) {
        if (parentWidth > parentHeight) {
            if (type == SHORT_MENU_PANE_TYPE)
                return (int) (parentWidth / 20);
            else if (type == MAIN_MENU_PANE_TYPE)
                return (int) (parentWidth / 3);
            else if (type == MENU_PANE_TYPE) {
                if (size >= 3 && index == 1)
                    return (int) (parentWidth / 3 - parentWidth / 15);
                else
                    return (int) (parentWidth / 3);
            }
            //else if (type == NORMAL_SHORT_PANE_TYPE)
            else if (type == NORMAL_PANE_TYPE && (Boolean)VData.GET().getVData(VData.SLIDED))
                return (int) (parentWidth - parentWidth / 3);
            else if (type == NORMAL_PANE_TYPE)
                return (int) (parentWidth);
            else if (type == DEFAULT_PANE_TYPE)
                return (int) (parentWidth / 3);
            else throw new IllegalStateException("Pane has unknown type");
        } else {
            if (type == SHORT_MENU_PANE_TYPE)
                return (int) (parentWidth / 20);
            else if (type == MAIN_MENU_PANE_TYPE)
                return (int) (parentWidth / 3);
            else if (type == MENU_PANE_TYPE) {
                if (size >= 3 && index == 1)
                    return (int) (parentWidth / 3 - parentWidth / 15);
                else
                    return (int) (parentWidth / 3);
            }
            //else if (type == NORMAL_SHORT_PANE_TYPE)
            else if (type == NORMAL_PANE_TYPE && (Boolean)VData.GET().getVData(VData.SLIDED))
                return (int) (parentWidth - parentWidth / 3);
            else if (type == NORMAL_PANE_TYPE)
                return (int) (parentWidth);
            else if (type == DEFAULT_PANE_TYPE)
                return (int) (parentWidth / 3);
                //if (type == DEFAULT_PANE_TYPE)
                //return (int) (0.75 * parentWidth);
            else throw new IllegalStateException("Pane has unknown type");
        }
    }

    @Override
    public int getType(Object o, int index, boolean lastInShort) {
        if (o instanceof ExampleFragment)
            return DEFAULT_PANE_TYPE;
        if (o instanceof MainMenuFragment) {
            if (index == 0)
                return MAIN_MENU_PANE_TYPE;
            else
                return MENU_PANE_TYPE;
        }
        if (o instanceof ShortMenuFragment)
            return SHORT_MENU_PANE_TYPE;
        //if (o instanceof NormalFragment && lastInShort)
            //return NORMAL_SHORT_PANE_TYPE;
        if (o instanceof NormalFragment)
            return NORMAL_PANE_TYPE;
        else return UNKNOWN_PANE_TYPE;
    }

    @Override
    public boolean getFocused(Object o) {
        return false;
    }
}
