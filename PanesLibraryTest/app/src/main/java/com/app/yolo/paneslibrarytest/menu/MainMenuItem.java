package com.app.yolo.paneslibrarytest.menu;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.app.yolo.paneslibrarytest.R;

import java.io.IOException;

/**
 * Created by yolo on 10/15/14.
 */
public class MainMenuItem {
    public MainMenuItem(Context context) { this.context = context; }
    public MainMenuItem(Context context, String text, String path, int progress) {
        this.context = context;
        this.text = text;
        this.setPicture(path);
        this.progress = progress;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setPicture(String path) {
        picture = null;
        try {
            picture = Drawable.createFromStream(context.getAssets().open(path), null);
        } catch (IOException e) {
            e.printStackTrace();
            picture = context.getResources().getDrawable(R.drawable.ic_launcher);
        }
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public String getText() {
        return text;
    }

    public Drawable getPicture() {
        return picture;
    }

    public int getProgress() {
        return progress;
    }

    /** A/D
     * picture      : The item's picture
     * text         : The item's text
     * progress     : The item's progress
     */
    private Context     context;
    private Drawable    picture;
    private String      text;
    private int         progress;
}
