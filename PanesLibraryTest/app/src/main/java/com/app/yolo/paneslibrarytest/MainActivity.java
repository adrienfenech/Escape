package com.app.yolo.paneslibrarytest;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.GestureDetector;
import android.view.MotionEvent;

import com.app.yolo.paneslibrarytest.androidpaneslibrary.paneslayout.PanesActivity;
import com.app.yolo.paneslibrarytest.menu.MainMenuFragment;
import com.app.yolo.paneslibrarytest.tools.MyPaneSizer;
import com.app.yolo.paneslibrarytest.tools.VData;


public class MainActivity extends PanesActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);

        setPaneSizer(new MyPaneSizer());

        // Lets setup a menu and a first pane!
        if (savedInstanceState == null) {
            Fragment menu = new MainMenuFragment();
            //Fragment shortMenu = new ShortMenuFragment();
            //Fragment first = new ExampleFragment();
            setMenuFragment(menu);
            //setShortMenuFragment(shortMenu);
            //addFragment(menu, first);

            // if you wanted to add more fragments after these ones, you can do:
            //Fragment second = new ExampleFragment();
            //Fragment third = new ExampleFragment();
            //addFragment(first, second);
            //addFragment(second, third);
        }
    }
}
