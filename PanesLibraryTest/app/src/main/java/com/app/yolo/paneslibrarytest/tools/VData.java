package com.app.yolo.paneslibrarytest.tools;

/**
 * Created by yolo on 10/16/14.
 */
public class VData {
    private static VData ourInstance = new VData();

    public static VData GET() {
        return ourInstance;
    }

    private VData() {
        depht = -1; slided = false;
    }

    public Object setVData(short key, Object o) {
        if (key == DEPHT)
            this.depht = (Integer)o;
        else if (key == SLIDED)
            this.slided = (Boolean)o;
        return o;
    }

    public Object getVData(short key) {
        if (key == DEPHT)
            return this.depht;
        else if (key == SLIDED) {
            boolean temp = slided;
            this.slided = false;
            return temp;
        }
        else
            return null;
    }

    /** A/D
     * depht        : Value use to know at which menu's depht we are (Changed only with menu)
     * slided       : True if screend slided and change to false when value getted
     */

    private Integer     depht;
    private Boolean     slided;

    /** Constant Value **/
    public static short     DEPHT   = 0x00000001;
    public static short     SLIDED  = 0x00000002;
}
