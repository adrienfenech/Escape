package com.app.yolo.paneslibrarytest.androidpaneslibrary.paneslayout;

import android.support.v4.app.Fragment;

public interface FragmentLauncher {
	
	public void addFragment(Fragment prevFragment, Fragment newFragment);
	
}
