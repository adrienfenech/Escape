package com.app.yolo.paneslibrarytest.menu;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.yolo.paneslibrarytest.R;

import java.util.ArrayList;

/**
 * Created by yolo on 10/15/14.
 */
public class MyMainMenuAdapter extends ArrayAdapter<MainMenuItem> {
    public MyMainMenuAdapter(Activity context, ArrayList<MainMenuItem> items) {
        super(context, R.layout.main_menu_item, items);
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.main_menu_item, null, true);
        TextView textView = (TextView) rowView.findViewById(R.id.main_menu_item_text);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.main_menu_item_picture);
        ProgressBar progressBar = (ProgressBar) rowView.findViewById(R.id.main_menu_item_progressbar);
        textView.setText(items.get(position).getText());
        imageView.setImageDrawable(items.get(position).getPicture());
        progressBar.setProgress(100);
        progressBar.setProgress(items.get(position).getProgress());
        return rowView;
    }

    /** A/D
     * context      : Context of the application
     * items        : List of the item of the main menu
     */
    final private Activity                  context;
    final private ArrayList<MainMenuItem>   items;
}
