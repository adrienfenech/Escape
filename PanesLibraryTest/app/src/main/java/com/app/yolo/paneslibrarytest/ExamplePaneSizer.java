package com.app.yolo.paneslibrarytest;

import com.app.yolo.paneslibrarytest.androidpaneslibrary.paneslayout.PanesSizer;

/**
 * Created by yolo on 10/14/14.
 */
public class ExamplePaneSizer implements PanesSizer.PaneSizer {
    @Override
    public int getWidth(int index, int size, int type, int parentWidth, int parentHeight) {
        return 0;
    }

    @Override
    public int getType(Object o, int index, boolean lastInShort) {
        return 0;
    }

    @Override
    public boolean getFocused(Object o) {
        return false;
    }
}
